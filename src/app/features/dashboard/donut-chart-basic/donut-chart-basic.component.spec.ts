import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonutChartBasicComponent } from './donut-chart-basic.component';

describe('DonutChartBasicComponent', () => {
  let component: DonutChartBasicComponent;
  let fixture: ComponentFixture<DonutChartBasicComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DonutChartBasicComponent]
    });
    fixture = TestBed.createComponent(DonutChartBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
