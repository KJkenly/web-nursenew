import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnChartBasicComponent } from './column-chart-basic.component';

describe('ColumnChartBasicComponent', () => {
  let component: ColumnChartBasicComponent;
  let fixture: ComponentFixture<ColumnChartBasicComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ColumnChartBasicComponent]
    });
    fixture = TestBed.createComponent(ColumnChartBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
